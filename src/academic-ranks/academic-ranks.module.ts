import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { AcademicRanksController } from './academic-ranks.controller';
import {
  AddNewAcademicRank,
  AddNewAcademicRankHandler,
} from './commands/add-new-academic-rank';
import {
  UpdateAcademicRank,
  UpdateAcademicRankHandler,
} from './commands/update-academic-rank';
import {
  GetAcademicRankByName,
  GetAcademicRankByNameHandler,
} from './queries/get-academic-rank-by-name';
import {
  GetAcademicRankList,
  GetAcademicRankListHandler,
} from './queries/get-academic-rank-list';
import { AcademicRank, AcademicRankSchema } from './schemas';

const queries = [GetAcademicRankList, GetAcademicRankByName];
const command = [AddNewAcademicRank, UpdateAcademicRank];
const handlers = [
  GetAcademicRankListHandler,
  GetAcademicRankByNameHandler,

  AddNewAcademicRankHandler,
  UpdateAcademicRankHandler,
];

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: AcademicRank.name,
        schema: AcademicRankSchema,
      },
    ]),
  ],
  controllers: [AcademicRanksController],
  providers: [...queries, ...command, ...handlers],
})
export class AcademicRanksModule {}
