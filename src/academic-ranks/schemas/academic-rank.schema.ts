import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AcademicRankDocument = AcademicRank & Document;

@Schema({ versionKey: false })
export class AcademicRank {
  @Prop()
  name!: string;
}

export const AcademicRankSchema = SchemaFactory.createForClass(AcademicRank);

AcademicRankSchema.set('toJSON', {
  transform: (_, result) => {
    const { _id, ...rest } = result;

    return {
      ...rest,
      id: _id,
    };
  },
});
