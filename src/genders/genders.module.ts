import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { AddNewGender, AddNewGenderHandler } from './commands/add-new-gender';
import { UpdateGender, UpdateGenderHandler } from './commands/update-gender';
import { GendersController } from './genders.controller';
import {
  GetGenderByName,
  GetGenderByNameHandler,
} from './queries/get-gender-by-name';
import { GetGenderList, GetGenderListHandler } from './queries/get-gender-list';
import { Gender, GenderSchema } from './schemas';

const queries = [GetGenderList, GetGenderByName];
const commands = [AddNewGender, UpdateGender];
const handlers = [
  GetGenderListHandler,
  GetGenderByNameHandler,

  AddNewGenderHandler,
  UpdateGenderHandler,
];

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([
      {
        name: Gender.name,
        schema: GenderSchema,
      },
    ]),
  ],
  controllers: [GendersController],
  providers: [...queries, ...commands, ...handlers],
})
export class GendersModule {}
