import { IsString } from 'class-validator';

export class GetEducationalLevelByName {
  @IsString()
  name!: string;
}
