import { IsString } from 'class-validator';

export class GetEducationalLevelById {
  @IsString()
  id!: string;
}
