import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import {
  AddNewEducationalGrade,
  AddNewEducationalGradeHandler,
} from './commands/add-new-educational-grade';
import {
  AddNewEducationalLevel,
  AddNewEducationalLevelHandler,
} from './commands/add-new-educational-level';
import { EducationalLevelsController } from './educational-levels.controller';
import {
  GetEducationalLevelById,
  GetEducationalLevelByIdHandler,
} from './queries/get-educational-level-by-id';
import {
  GetEducationalLevelByName,
  GetEducationalLevelByNameHandler,
} from './queries/get-educational-level-by-name';
import {
  EducationalGrade,
  EducationalGradeSchema,
  EducationalLevel,
  EducationalLevelSchema,
} from './schemas';
import { EducationalGradesController } from './educational-grades.controller';
import {
  UpdateEducationalLevel,
  UpdateEducationalLevelHandler,
} from './commands/update-academic-level';

const queries = [GetEducationalLevelById, GetEducationalLevelByName];
const commands = [
  AddNewEducationalLevel,
  UpdateEducationalLevel,
  AddNewEducationalGrade,
];

const commandHandlers = [
  GetEducationalLevelByIdHandler,
  GetEducationalLevelByNameHandler,

  AddNewEducationalLevelHandler,
  UpdateEducationalLevelHandler,
  AddNewEducationalGradeHandler,
];

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([
      { name: EducationalLevel.name, schema: EducationalLevelSchema },
      { name: EducationalGrade.name, schema: EducationalGradeSchema },
    ]),
  ],
  controllers: [EducationalLevelsController, EducationalGradesController],
  providers: [...queries, ...commands, ...commandHandlers],
})
export class EducationalLevelsModule {}
