import {
  Body,
  Controller,
  NotFoundException,
  Param,
  Post,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { AddNewEducationalGrade } from './commands/add-new-educational-grade';
import { GetEducationalLevelById } from './queries/get-educational-level-by-id';

@Controller('educational-levels/:educationalLevelId/educational-grades')
export class EducationalGradesController {
  constructor(private readonly _commandBus: CommandBus) {}

  @Post(':id')
  public async addEducationalGrade(
    @Param('educationalLevelId')
    educationalLevelId: string,
    @Body() addNewEducationalGrade: AddNewEducationalGrade,
  ) {
    const getEducationalLevelById = new GetEducationalLevelById();
    getEducationalLevelById.id = educationalLevelId;
    const educationalLevel = await this._commandBus.execute(
      getEducationalLevelById,
    );

    if (!educationalLevel)
      throw new NotFoundException(
        `Educational level with id ${educationalLevelId} cannot be found`,
      );

    addNewEducationalGrade.educationalLevelId = educationalLevelId;
    const educationalGrade = await this._commandBus.execute(
      addNewEducationalGrade,
    );

    return educationalGrade;
  }
}
