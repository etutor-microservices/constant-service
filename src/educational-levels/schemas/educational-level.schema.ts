import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export type EducationalLevelDocument = EducationalLevel & mongoose.Document;

@Schema({ versionKey: false })
export class EducationalLevel {
  @Prop({ unique: true })
  name!: string;
}

export const EducationalLevelSchema =
  SchemaFactory.createForClass(EducationalLevel);

EducationalLevelSchema.set('toJSON', {
  transform: (_, result) => {
    const { _id, ...rest } = result;

    return {
      ...rest,
      id: _id,
    };
  },
});
