import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { AddNewEducationalGrade } from './commands/add-new-educational-grade/add-new-educational-grade';
import { AddNewEducationalLevel } from './commands/add-new-educational-level';
import { UpdateEducationalLevel } from './commands/update-academic-level';
import { GetEducationalLevelById } from './queries/get-educational-level-by-id';
import { GetEducationalLevelByName } from './queries/get-educational-level-by-name';

@Controller('educational-levels')
export class EducationalLevelsController {
  constructor(private readonly _commandBus: CommandBus) {}

  @Get(':id')
  public async getById(
    @Param() getEducationalLevelById: GetEducationalLevelById,
  ) {
    const educationalLevel = await this._commandBus.execute(
      getEducationalLevelById,
    );

    if (!educationalLevel)
      throw new NotFoundException(
        `Educational level with id ${getEducationalLevelById.id} cannot be found`,
      );

    return educationalLevel;
  }

  @Post()
  public async createNew(
    @Body() createNewEducationalLevel: AddNewEducationalLevel,
  ) {
    const getEducationalLevelByName = new GetEducationalLevelByName();
    getEducationalLevelByName.name = createNewEducationalLevel.name;
    const educationalLevel = await this._commandBus.execute(
      getEducationalLevelByName,
    );

    if (!!educationalLevel)
      throw new BadRequestException(
        `Educational level ${createNewEducationalLevel.name} has already exist.`,
      );

    return this._commandBus.execute(createNewEducationalLevel);
  }

  @Patch(':id')
  public async update(
    @Param('id') id: string,
    @Body() updateEducationalLevel: UpdateEducationalLevel,
  ) {
    const [isExist, educationalLevel] = await this._commandBus.execute(
      updateEducationalLevel,
    );

    if (isExist)
      throw new BadRequestException(
        `Educational level ${updateEducationalLevel.name} has already exist.`,
      );

    if (!educationalLevel)
      throw new NotFoundException(
        `Educational level with id ${id} cannot be found.`,
      );

    return educationalLevel;
  }
}
