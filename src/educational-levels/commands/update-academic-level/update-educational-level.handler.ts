import { ICommandHandler } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EducationalLevel } from '../../schemas';
import { UpdateEducationalLevel } from './update-educational-level';

export class UpdateEducationalLevelHandler
  implements
    ICommandHandler<UpdateEducationalLevel, [boolean, EducationalLevel | null]>
{
  constructor(
    @InjectModel(EducationalLevel.name)
    private readonly _educationalLevelModel: Model<EducationalLevel>,
  ) {}

  /**
   *
   * @returns {exist, educationalLevel}
   */
  async execute(
    command: UpdateEducationalLevel,
  ): Promise<[boolean, EducationalLevel | null]> {
    let updateClause = {};

    if (!!command.name) {
      const educationalLevel = await this._educationalLevelModel
        .where({
          name: command.name.toLocaleLowerCase(),
        })
        .findOne()
        .exec();
      if (!!educationalLevel) return [true, null];

      updateClause = { name: command.name.toLocaleLowerCase() };
    }

    return [
      false,
      await this._educationalLevelModel.findByIdAndUpdate(
        command.id,
        updateClause,
        { returnOriginal: false },
      ),
    ];
  }
}
