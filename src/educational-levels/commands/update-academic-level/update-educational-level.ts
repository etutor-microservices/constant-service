import { PartialType } from '@nestjs/mapped-types';
import { AddNewEducationalLevel } from '../add-new-educational-level';

export class UpdateEducationalLevel extends PartialType(
  AddNewEducationalLevel,
) {
  id!: string;
}
