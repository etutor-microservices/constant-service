import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import { DatabaseModule } from './database/database.module';
import { EducationalLevelsModule } from './educational-levels/educational-levels.module';
import { AcademicRanksModule } from './academic-ranks/academic-ranks.module';
import { GendersModule } from './genders/genders.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        PORT: Joi.number().required(),

        DATABASE_USER: Joi.string().required(),
        DATABASE_PASS: Joi.string().required(),
        DATABASE_URI: Joi.string().required(),
        DATABASE_NAME: Joi.string().required(),
      }),
    }),

    DatabaseModule,
    EducationalLevelsModule,
    AcademicRanksModule,
    GendersModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
